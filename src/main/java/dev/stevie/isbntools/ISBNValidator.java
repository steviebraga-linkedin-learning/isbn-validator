package dev.stevie.isbntools;

public class ISBNValidator {

    private static final int SHORT_ISBN_LENGTH = 10;
    private static final int SHORT_ISBN_MULTIPLIER = 11;
    private static final int LONG_ISBN_LENGTH = 13;
    private static final int LONG_ISBN_MULTIPLIER = 10;

    public boolean checkISBN(String isbn) {
        if (isbn != null && (isbn.length() == SHORT_ISBN_LENGTH || isbn.length() == LONG_ISBN_LENGTH)) {
            return isbn.length() == SHORT_ISBN_LENGTH ? checkShortISBN(isbn) : checkLongISBN(isbn);
        } else {
            throw new IllegalArgumentException("ISBN numbers must have 10 or 13 digits length!");
        }
    }

    private boolean checkShortISBN(String isbn) {
        int sum = 0;

        for (int i = 0; i < SHORT_ISBN_LENGTH; i++) {
            if (!Character.isDigit(isbn.charAt(i))) {
                if (i == 9 && isbn.charAt(i) == 'X') {
                    sum += 10;
                } else {
                    throw new NumberFormatException("ISBN numbers must contain only numeric digits or letter X!");
                }
            } else {
                sum += (SHORT_ISBN_LENGTH - i) * Character.getNumericValue(isbn.charAt(i));
            }
        }

        return sum % SHORT_ISBN_MULTIPLIER == 0;
    }

    private boolean checkLongISBN(String isbn) {
        int sum = 0;

        for (int i = 0; i < LONG_ISBN_LENGTH; i++) {
            if (i % 2 != 0) {
                sum += 3 * Character.getNumericValue(isbn.charAt(i));
            } else {
                sum += Character.getNumericValue(isbn.charAt(i));
            }
        }

        return sum % LONG_ISBN_MULTIPLIER == 0;
    }

}
