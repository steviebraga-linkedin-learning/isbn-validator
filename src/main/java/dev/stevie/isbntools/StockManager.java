package dev.stevie.isbntools;

public class StockManager {

    private ExternalISBNDataService webService;
    private ExternalISBNDataService databaseService;

    public String getLocatorCode(String isbn) {
        Book book = databaseService.lookup(isbn);
        if (book == null) {
            book = webService.lookup(isbn);
        }
        StringBuilder locatorBuilder = new StringBuilder();
        locatorBuilder.append(isbn.substring(isbn.length() - 4, isbn.length()));
        locatorBuilder.append(book.getAuthor().substring(0, 1));
        locatorBuilder.append(book.getTitle().split(" ").length);
        return locatorBuilder.toString();
    }

    public void setWebService(ExternalISBNDataService webService) {
        this.webService = webService;
    }

    public void setDatabaseService(ExternalISBNDataService databaseService) {
        this.databaseService = databaseService;
    }
}
