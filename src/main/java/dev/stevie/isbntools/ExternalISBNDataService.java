package dev.stevie.isbntools;

public interface ExternalISBNDataService {

    Book lookup(String isbn);

}
