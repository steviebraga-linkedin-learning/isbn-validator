package dev.stevie.isbntools;

import org.junit.Test;

import static org.junit.Assert.*;

public class ISBNValidatorTest {

    @Test
    public void checkAValidISBN() {
        ISBNValidator validator = new ISBNValidator();
        boolean result = validator.checkISBN("0140449116");
        assertTrue(result);

        result = validator.checkISBN("9780321125217");
        assertTrue(result);
    }

    @Test
    public void checkAnInvalidISBN() {
        ISBNValidator validator = new ISBNValidator();
        boolean result = validator.checkISBN("0140449117");
        assertFalse(result);

        result = validator.checkISBN("9780321125218");
        assertFalse(result);
    }

    @Test(expected = IllegalArgumentException.class)
    public void nullValuesArentAllowed() {
        ISBNValidator validator = new ISBNValidator();
        validator.checkISBN(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void nineDigitISBNAreNotAllowed() {
        ISBNValidator validator = new ISBNValidator();
        validator.checkISBN("123456789");
    }

    @Test(expected = NumberFormatException.class)
    public void checkForISBNWithNonDigitCharacters() {
        ISBNValidator validator = new ISBNValidator();
        validator.checkISBN("helloworld");
    }

    @Test
    public void checkValidISBNNumbersEndingWithX() {
        ISBNValidator validator = new ISBNValidator();
        boolean result = validator.checkISBN("012000030X");
        assertTrue(result);
    }

}
