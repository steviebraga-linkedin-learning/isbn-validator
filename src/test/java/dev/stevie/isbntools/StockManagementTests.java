package dev.stevie.isbntools;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class StockManagementTests {

    private ExternalISBNDataService testWebService;
    private ExternalISBNDataService testDatabaseService;
    private StockManager stockManager;

    @Before
    public void setUp() {
        testWebService = mock(ExternalISBNDataService.class);
        testDatabaseService = mock(ExternalISBNDataService.class);
        stockManager = new StockManager();

        stockManager.setWebService(testWebService);
        stockManager.setDatabaseService(testDatabaseService);
    }

    @Test
    public void testCanGetCorrectLocatorCode() {
        when(testWebService.lookup("0140177396")).thenReturn(new Book("0140177396", "Of Mice And Men", "J. Steinbeck"));
        when(testDatabaseService.lookup("0140177396")).thenReturn(null);

        String isbn = "0140177396";
        String locatorCode = stockManager.getLocatorCode(isbn);
        assertEquals("7396J4", locatorCode);
    }

    @Test
    public void databaseIsUsedIfDataIsPresent() {
        when(testDatabaseService.lookup("0140177396")).thenReturn(new Book("0140177396", "ABC", "ABC"));

        String isbn = "0140177396";
        stockManager.getLocatorCode(isbn);

//        verify(databaseService, times(1)).lookup("0140177396");
        verify(testDatabaseService).lookup("0140177396");

//        verify(webService, times(0)).lookup(anyString());
        verify(testWebService, never()).lookup(anyString());
    }

    @Test
    public void webServiceIsUsedIfDataIsNotPresent() {
        when(testDatabaseService.lookup("0140177396")).thenReturn(null);
        when(testWebService.lookup("0140177396")).thenReturn(new Book("0140177396", "ABC", "ABC"));

        String isbn = "0140177396";
        stockManager.getLocatorCode(isbn);

//        verify(databaseService, times(1)).lookup("0140177396");
        verify(testDatabaseService).lookup("0140177396");

//        verify(webService, times(1)).lookup("0140177396");
        verify(testWebService).lookup("0140177396");
    }

}
