package dev.stevie.tautologies;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class NumberValidatorTests {

    private NumberValidator numberValidator;

    @Before
    public void setUp() {
        numberValidator = new NumberValidator();
    }

    @Test
    public void checkPrimeNumbers() {
        Integer numbers[] = {1, 23, 61, 79};

        for (int i = 0; i < numbers.length; i++) {
            assertTrue(numberValidator.isItPrime(numbers[i]));
        }
    }

    @Test
    public void checkNonPrimeNumbers() {
        Integer numbers[] = {15, 25, 60, 63, 207};

        for (int i = 0; i < numbers.length; i++) {
            assertFalse(numberValidator.isItPrime(numbers[i]));
        }
    }

}
